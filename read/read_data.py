data_html = {
    'statements': '''<!DOCTYPE html>\n<html>\n<head>\n    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>\n    <title>Statements</title>\n\n    <meta http-equiv="X-UA-Compatible" content="IE=edge">\n    <meta name="description" content="">\n    <meta name="viewport" content="width=device-width,initial-scale=1">\n    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">\n    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">\n    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">\n    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>\n    <style>\n        nav .nav-wrapper {\n            background-color: #26a69a;\n        }\n    </style>\n</head>\n<body>\n    <div class="row">\n        <nav>\n            <div class="nav-wrapper">\n            <a href="/account" class="brand-logo">Home</a>\n            <ul id="nav-mobile" class="right hide-on-med-and-down">\n                <li><a href="/customer">Settings</a></li>\n                <li><a href="/logout">Logout</a></li>\n            </ul>\n            </div>\n        </nav>\n        <div class="col s6 offset-s3">\n            <table class="striped">\n                <thead>\n                    <tr>\n                        <th>Statement</th>\n                        <th>Date</th>\n                        <th>Amount</th>\n                        <th>Balance</th>\n                    </tr>\n                </thead>\n                <tbody>\n                    \n                    <tr>\n                        <td>Bar Pepe</td>\n                        <td>05/07/2018</td>\n                        <td class="red-text text-darken-2">€30</td>\n                        <td >€352</td>\n                    </tr>\n                    \n                    <tr>\n                        <td>Transferencia</td>\n                        <td>15/06/2018</td>\n                        <td >€100</td>\n                        <td >€382</td>\n                    </tr>\n                    \n                    <tr>\n                        <td>Compra online</td>\n                        <td>02/06/2018</td>\n                        <td class="red-text text-darken-2">€20</td>\n                        <td >€282</td>\n                    </tr>\n                    \n                    <tr>\n                        <td>Transferencia</td>\n                        <td>11/05/2018</td>\n                        <td >€80</td>\n                        <td >€302</td>\n                    </tr>\n                    \n                    <tr>\n                        <td>Retiro cajero</td>\n                        <td>01/04/2018</td>\n                        <td class="red-text text-darken-2">€30</td>\n                        <td >€222</td>\n                    </tr>\n                    \n                    <tr>\n                        <td>Bar Pepe</td>\n                        <td>03/01/2018</td>\n                        <td class="red-text text-darken-2">€30</td>\n                        <td >€252</td>\n                    </tr>\n                    \n                </tbody>\n            </table>\n        </div>\n    </div>\n</body>\n</html>''',
    'customer': '''<!DOCTYPE html>\n<html>\n<head>\n    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>\n    <title>Settings</title>\n\n    <meta http-equiv="X-UA-Compatible" content="IE=edge">\n    <meta name="description" content="">\n    <meta name="viewport" content="width=device-width,initial-scale=1">\n    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">\n    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">\n    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">\n    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>\n    <style>\n        nav .nav-wrapper {\n            background-color: #26a69a;\n        }\n    </style>\n</head>\n<body>\n    <div class="row">\n        <nav>\n            <div class="nav-wrapper">\n            <a href="account" class="brand-logo">Home</a>\n            <ul id="nav-mobile" class="right hide-on-med-and-down">\n                <li><a href="customer">Settings</a></li>\n                <li><a href="logout">Logout</a></li>\n            </ul>\n            </div>\n        </nav>\n        <div class="col s6 offset-s3">\n            <ul class="collection with-header">\n                <li class="collection-header"><h4>Pepito Perez</h4></li>\n                <li class="collection-item">+34 644323221</li>\n                <li class="collection-item">pepito@perez.com</li>\n                <li class="collection-item">Carrer de Girona, 90, 08009 Barcelona</li>\n            </ul>\n        </div>\n    </div>\n</body>\n</html>''',
    'account': '''<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <title>Accounts</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
    <style>
        nav .nav-wrapper {
            background-color: #26a69a;
        }
    </style>
</head>
<body>
    <div class="row">
        <nav>
            <div class="nav-wrapper">
            <a href="account" class="brand-logo">Home</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="customer">Settings</a></li>
                <li><a href="logout">Logout</a></li>
            </ul>
            </div>
        </nav>
        <div class="col s6 offset-s3">
            <h4>Accounts</h4>
            <ul class="collection">

                <li class="collection-item avatar">
                    <span class="title">Cuenta personal</span>
                    <p>ES232100123303030032 <br>
                    <span >€352</span>
                    </p>
                    <a href="statements/1" class="secondary-content"><i class="material-icons">info_outline</i></a>
                </li>

                <li class="collection-item avatar">
                    <span class="title">Cuenta ahorro</span>
                    <p>ES232100523522355235 <br>
                    <span >€1322.2</span>
                    </p>
                    <a href="statements/2" class="secondary-content"><i class="material-icons">info_outline</i></a>
                </li>

            </ul>
        </div>
    </div>
</body>
</html>
''',


}


data_dict = {
    'Accounts': 2,
    'Account Data List': [
        # first account
        {
            'Name': 'Cuenta personal',
            'Number': 'ES232100123303030032',
            'Currency': 'EUR',
            'Balance': 352,
            'Total customers': 1,
            'Customer Data List': [
                {'Name': 'Pepito Perez',
                 'Participation': 'Titular',
                 'Doc': 'Y01235813',
                 'Address': 'Carrer de Girona, 90, 08009 Barcelona',
                 'Emails': 'pepito@perez.com',
                 'Phones': '+34 600000000',
                 },
            ]
        },
        # second account
        {
            'Name': 'Cuenta ahorro',
            'Number': 'ES232100523522355235',
            'Currency': 'EUR',
            'Balance': 1_322.22
        },


    ],
    'Customer': {
        'Customer Data List':
            {'Name': 'Pepito Perez',
             'Participation': 'Titular',
             'Doc': 'Y01235813',
             'Address': 'Carrer de Girona, 90, 08009 Barcelona',
             'Emails': 'pepito@perez.com',
             'Phones': '+34 600000000',
             },

    },

    # Total customers: 1
    #         Customer Data:
    #                 Name: Pepito Perez
    #                 Participation: Titular
    #                 Doc: Y01235813

    #                 Address: Carrer de Girona, 90, 08009 Barcelona

    #                 Emails: pepito@perez.com
    #                 Phones: +34 600000000

    # Statements(6)

}
