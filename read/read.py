import argparse
import requests
import sys
# import json
import logging
from bs4 import BeautifulSoup


# TODO make a class!!!!
# TODO switch 'script' folder into a package
# set logger for logs collection
logging.basicConfig(
    filename='read.logs',
    format='%(asctime)s %(levelname)-8s %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    level=logging.INFO,
)
logger = logging.getLogger()


# connection parameters
URL = 'https://test.unnax.com/'
LOGIN_PAGE = 'login'
FIRST_PAGE = 'account'

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Origin': URL,
    'Connection': 'keep-alive',
    'Referer': URL + LOGIN_PAGE,
    'Upgrade-Insecure-Requests': '1',
}


class Read():
    ''' provide login, and session creation for sucessive requests
        provide also staticmethods to parse the html pages
    '''

    def __init__(self, user, pw, url=URL,
                 login_page=LOGIN_PAGE,
                 first_page=FIRST_PAGE):
        self.user = user
        self.pw = pw
        self.url = url
        self.login = login_page
        self.first_page = first_page
        self.s = requests.Session()

    @staticmethod
    def input_parser_get_user_and_pw() -> tuple:
        ''' read user and pw data introduced by command line
            IN   linux command line text
            OUT  tuple with user and pw
        '''
        user, pw = '', ''
        try:
            parser = argparse.ArgumentParser()
            parser.add_argument("-u", "--username", required=True)
            parser.add_argument("-p", "--password", required=True)
            args = parser.parse_args()
            print(args)
            user = args.username
            pw = args.password
        except:
            msg = 'Invalid input: review the launching command line statement'
            logger.error(msg)
            sys.exit(msg)
        return tuple([user, pw])

    def log_in_and_create_session(self):
        '''
        create a session for user
        '''
        data = {
            'username': self.user,
            'password': self.pw,
        }

        self.s.post(self.url + self.login,
                    headers=headers,
                    data=data)

        # TODO review invalid check of session creation
        accounts_page = self.s.get(self.url + self.first_page)
        if accounts_page.status_code != 200:
            msg = 'Invalid credentials'
            logger.error(msg)
            sys.exit(msg)

    def fetch_data_to_dict(self) -> dict:
        ''' scrap the webpage: crowling + parsing
        Provide a dictionary of data, based of custom webpages parser
        (the following static methods)    
        '''
        res = {}
        logger.info('Start scraping the webpage')

        accounts_page = self.s.get(self.url + self.first_page)
        accounts = self.extract_account_info(accounts_page.text)
        res['Account Data Links'] = accounts

        # TODO extract account info from nested parsing
        res['Account Data List'] = [
            {
                'Name': 'Cuenta personal',
                'Number': 'ES232100123303030032',
            },
            {
                'Name': 'Cuenta ahorro',
                'Number': 'ES232100123303030032',
            },
        ]

        customer = self.s.get(self.url + 'customer')
        res['Customer'] = self.extract_customer_info(customer.text)

        res['Accounts'] = len(accounts)

        self.data = res
        return res

    @staticmethod
    def extract_account_info(html) -> dict:
        ''' from main page list the account url'''
        res = {}
        soup = BeautifulSoup(html, features="lxml")
        print(soup.prettify())

        account_page_list = []
        links = soup.select(".secondary-content")
        for a in links:
            print("Found the URL:", a['href'])
            account_page_list.append(a['href'])

        res['Account URL List'] = account_page_list
        return res

    @staticmethod
    def extract_customer_info(html) -> dict:
        ''' from settings page fecth the user information '''
        res = {}
        soup = BeautifulSoup(html, features="lxml")
        # print(soup.prettify())
        samples = soup.select(".collection-item")
        res['Customer Data List'] = {
            'Name': soup.select_one('.collection-header').get_text(),
            'Address': samples[2].get_text(),
            'Emails': samples[1].get_text(),
            'Phones': samples[0].get_text(),
        }
        print(res)
        return res

    @ staticmethod
    def data_parser_to_str(data_dict) -> str:
        ''' provide a human readeble output for
        data dictionary containing accounts info
            IN  data_dict
            OUT str
        '''

        logger.info('Executing dict to string transformation')
        res = ''

        if len(data_dict) == 0:
            msg = 'No data to parse: check scraping process'
            print(msg)
            logger.error(msg)
            return res

        res += f"\tAccounts ( {data_dict['Accounts']} )\n"
        for account in data_dict['Account Data List']:
            res += '\t\t\tAccount Data:\n'
            res += f"\t\t\t\t\tName: {account['Name']}\n"
            res += f"\t\t\t\t\tNumber: {account['Number']}\n"

        customer_data = data_dict['Customer']['Customer Data List']
        res += f"\t\t\t'Name': {customer_data['Name']}\n"
        res += f"\t\t\t'Address': {customer_data['Address']}\n"
        
        # TODO continue the whole file
        # TODO search again a module (like pprint) that can format the dict

        return res


if __name__ == '__main__':

    # print account data in case of direct execution 
    username, password = Read.input_parser_get_user_and_pw()
    r = Read(username, password)
    r.log_in_and_create_session()
    data_dict = r.fetch_data_to_dict()
    print(Read.data_parser_to_str(data_dict))
