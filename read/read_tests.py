import pytest
import sys
import requests
from read.read_data import data_dict
from read.read_data import data_html

from read.read import Read
# from read import fetch_data
# from read import input_parser


# TODO pass those cases to @pytest.mark.parametrize

URL = 'https://www.testjsonapi.com/users/'  # TODO change for real one...

data = {
    'username': 'Y3216434F',
    'password': 'pperez2018'
}

read_obj = Read(data['username'], data['password'])


def test_data_parser_to_str():
    ''' check the formatted string that is elaborated starting
    from a dict input '''

    assert Read.data_parser_to_str({}) == ''

    assert Read.data_parser_to_str(data_dict)[:11] == '\tAccounts ('

    res = Read.data_parser_to_str(data_dict).split('\n')
    assert res[1] == '\t\t\tAccount Data:'
    assert res[2] == '\t\t\t\t\tName: Cuenta personal'
    assert res[3] == '\t\t\t\t\tNumber: ES232100123303030032'


def test_log_in_and_create_session():
    read_obj.log_in_and_create_session()
    assert read_obj.s.verify == True

def test_extract_customer_info():
    r = Read.extract_customer_info(data_html['customer'])['Customer Data List']
    assert r['Phones'] == '+34 644323221'
    # assert r['Name'] == 'Pepito Perez'
   

def test_extract_accounts_info():
    r = Read.extract_account_info(data_html['account'])['Account URL List']
    assert r[0] == 'statements/1'
    assert r[1] == 'statements/2'