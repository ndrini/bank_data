from flask import Flask, request, jsonify
from pymongo import MongoClient
from celery import Celery
from flask_cors import CORS
import read  # a scraping script that get a dict with
             # user data info

app = Flask(__name__)
# app.config['DEBUG'] = True
celery = Celery(
    'task',
    brocker='amqp://guest@localhost//',
    backend='redis://localhost/',
)

CORS(app)


@app.route('/read/<code>', methods=['POST'])
@celery.task()
def data_fetch():
    ''' read back data of user'''
    user = request.json['username']
    pw = request.json['password']

    new_task = get_bank_data.delay([user, pw], task_id=code))
    return new_task

@ app.route('/read/<code>', methods = ['GET'])
@ celery.task()
def data_show():
    ''' return data of the users '''
    res = new_task.get(task_id = code)
    return jsonify(res)
