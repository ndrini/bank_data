# 1. Bank data introduction

Back-end implementation of a data retrieve and analys of a back application. 

## 1.1 Step 1
Make a python script that allows you to login to the test bench at http://test.unnax.com/, and that prints the user's information, their accounts and movements.

The script should be able to run as follows:

    python read.py --username Y3216434F --password pperez2018

And the expected result in the terminal would be the following:

    Accounts ( 2 )
            Account Data:
                    Name: Cuenta personal
                    Number: ES232100123303030032
                    Currency: EUR
                    Balance: 352

            Total customers: 1
                    Customer Data:
                            Name: Pepito Perez
                            Participation: Titular
                            Doc: Y01235813

                            Address: Carrer de Girona, 90, 08009 Barcelona

                            Emails: pepito@perez.com
                            Phones: +34 600000000

            Statements ( 6 )
                    Date          |  Amount    | Balance | Concept

                    2018-07-05 |    -30.0      |  352      | Bar Pepe

                    2018-06-15 |    100.0     |  382      | Transferencia

                    2018-06-02 |    -20.0      |  282      | Compra online

                    2018-05-11 |    80.0       |  302      | Transferencia

                    2018-04-01 |    -30.0      |  222      | Retiro cajero

                    2018-01-03 |    -30.0      |  252      | Bar Pepe

            Account Data:
                    Name: Cuenta ahorro
                    Number: ES232100523522355235
                    Currency: EUR
                    Balance: 1322.2

            Total customers: 1
                    Customer Data:
                            Name: Pepito Perez
                            Participation: Titular
                            Doc: Y01235813

                            Address: Carrer de Girona, 90, 08009 Barcelona

                            Emails: pepito@perez.com
                            Phones: +34 600000000


            Statements ( 5 )
                    Date          |  Amount    | Balance   | Concept

                    2018-07-25 |    -12.0      |  1322.2    | McDonalds

                    2018-07-21 |    280.0     |  1334.2    | Nomina

                    2018-02-12 |    280.0     |  1054.2    | Nomina

                    2018-01-01 |    -20.0      |  774.2     | Compra online

                    2017-07-11 |    -20.0      |  794.2     | Compra online

## 1.2 Step 2

Implement the error logs and handle exceptions. Implement unit tests and follow SOLID principles and naming conventions. You can use any Unit testing and Mocking framework you want.

Implementar logs de error y manejar excepciones. Implementar unit tests y seguir convenciones de nombres y principios SOLID. Se puede utilizar cualquier framework de Unit test y Mocking que se desee.

## 1.3 Step 3

Create an api in flask or django that allows you to store and consult the information collected similar to point 1. For this, it is recommended to use a queuing system that manages the tasks of reading the received accounts.

The api will contain the following endpoints:

POST / read / $ {CODE} data = {username, password}

It will start the reading process with the data provided

GET / read / $ {CODE}

It will return the reading in case of completion or an error message.

response => {

    status: DONE | ERROR | PENDING,

    data: {

        accounts: [{name, number, currency, balance},…]

        customers: [{name, participation, document, address, emails, phones}, ...]

        statements: [{date, amount, balance, concept},…]

    }

}

$ {CODE} is an identifier of the request and should not be reusable for a new read.

Consistently use HTTP status codes {200, 201, 400, 404, 500, etc}


# 1.4 Step 4

Implement the service from point 3 using docker-compose. In the docker-compose there should be all the dependencies used by the service from point 3, such as database (mysql, postgresql, mongodb), queue system (beanstalkd, rabbitmq), cache (redis), etc. 


=======================================

Crear una api en flask o django que permita almacenar y consultar la información recolectada similar al punto 1. Para ello se recomienda utilizar un sistema de colas que gestione las tareas de lectura de cuentas recibidas.

La api contendrá los siguientes endpoints:

POST /read/${CODE}  data={username, password}
Iniciará el proceso de lectura con los datos suministrados


GET /read/${CODE}
Retornará la lectura en caso de haberse completado o un mensaje de error.

response => {

    status: DONE | ERROR | PENDING,

    data: {

        accounts: [ {name, number, currency, balance}, … ]

        customers: [ {name, participation, document, address, emails, phones}, … ]

        statements: [ {date, amount, balance, concept}, … ]

    }

}

${CODE} es un identificador de la petición y no debería poder reutilizarse para una nueva lectura.

Usar de forma coherente los códigos de estado HTTP {200, 201, 400, 404. 500, etc} 





# Resolutin

## set up

If not docket composer used, set up with requitements

    virtual env

## Execution step 1
run 
    $ python3 read.py --username Nandan  --password ciao
see the results on stdout (console)


## Execution step 2
run 
    $ python3 read.py --username Nandan  --password ciao
see the results on stdout (console)

Punto 4.

Implementar el servicio del punto 3 usando docker-compose. En el docker-compose deberían estar todas las dependencias utilizadas por el servicio del punto 3, tales como database (mysql, postgresql, mongodb), queue system (beanstalkd, rabbitmq), cache (redis), etc.


Everyone loves alphabet soup.  And of course, you want to know if you can construct a message from the letters found in your bowl. Your Task: Write a function that takes as input two strings: 

- the message you want to write
- all the letters found in your bowl of alphabet soup.

The function should determine if you can write your message with the letters found in your bowl of soup. The <b>function should return True or False</b> accordingly.

# 2 Assumptions 

- The letters are ordered randomly.
- There is no guarantee that each letter occurs a similar number of times - indeed some letters might be missing entirely.
- It may be a very large bowl of soup containing many letters.


## 1.2 Desired Result 
Try to make your function efficient.  

Please use Big-O notation to explain how long it takes your function to run in terms of the length of your message (m) and the number of letters in your bowl of soup (s). You may write the function in any programming language you prefer - but please consider the language required by the position for which you are applying. 

# 2. Proposed Solution

## 2.1 Logic of the Proposed Solution 

As Insight in the solution. 

So, not possible to 

simply sort both string and compare them 


    if sorted(msg) != sorted(letters):
        return False

    example 


    >>> s2 = ['x', 'x', 'y', 'y', 'y', 'y', 'y', 'y']
    >>> s1 = ['x', 'y']
    >>> s1 in s2
    False



### caveat 

As known Python requieres attention for some special characters con sidere  esavlaber like \ 

so if the \ char is used either escape using \\ or use the row string. so thr funcion will return true if the input is  r'123\456' and '1\\23456' (as expected).


# code

## tests

install pytest or use docker environment
(access by )

    $ pytest -s alphabet.py 
     
    use -s option to have a verbose run of the tests
    


# function

Run the funtion, in Linux, with python3 installed software, using command line: 
    
    $ s1=Cammello 
    $ s2=ollemmaC
    $ python3 alphabet.py $s1 $s2
    True

